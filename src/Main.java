import java.io.File;
import java.io.FileNotFoundException;
import java.net.UnknownHostException;

import com.alphadevs.isslive.ISSClientHandler;
import com.alphadevs.isslive.LSHandlerConfig;
import com.alphadevs.isslive.LightStreamerClient;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.lightstreamer.ls_client.PushConnException;
import com.lightstreamer.ls_client.PushServerException;
import com.lightstreamer.ls_client.PushUserException;
import com.lightstreamer.ls_client.SubscrException;
import com.mongodb.MongoException;

public class Main {

	/**
	 * @param args
	 * @throws FileNotFoundException
	 * @throws JsonSyntaxException
	 * @throws JsonIOException
	 * @throws PushConnException
	 * @throws PushUserException
	 * @throws PushServerException
	 * @throws SubscrException
	 * @throws MongoException 
	 * @throws UnknownHostException 
	 */
	public static void main(String[] args) throws JsonIOException,JsonSyntaxException, FileNotFoundException, SubscrException,PushServerException, PushUserException, PushConnException, UnknownHostException, MongoException {
		LightStreamerClient client = new LightStreamerClient();
		//URL url = ClassLoader.getSystemClassLoader().getResource("org/jboss/netty/example/echo/sensors_config.json");
		client.start(new ISSClientHandler(LSHandlerConfig.readSensorConfig(new File("/Users/karthik/dev/workspace/isslive-stream-client/src/com/alphadevs/isslive/sensors_config.json"))));
	}
}
