package com.alphadevs.isslive;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

/**
 * 
 * LightStream handler config understands the groups and symbols the dataset is registered to
 * @author AlphaDevs
 *
 */

public class LSHandlerConfig {
	private String[] groups;
	private String[] symbols;

	public String[] getGroups() {
		return groups;
	}

	public void setGroups(String[] groups) {
		this.groups = groups;
	}

	public String[] getSymbols() {
		return symbols;
	}

	public void setSymbols(String[] symbols) {
		this.symbols = symbols;
	}

	public LSHandlerConfig() {

	}

	public static LSHandlerConfig readSensorConfig(File file)
			throws JsonIOException, JsonSyntaxException, FileNotFoundException {
		return SensorConfigParser.prase(file);
	}

}

class SensorConfigParser {

	public static LSHandlerConfig prase(File file) throws JsonIOException,JsonSyntaxException, FileNotFoundException {
		LSHandlerConfig result = new LSHandlerConfig();
		JsonParser p = new JsonParser();
		JsonElement parse = p.parse(new FileReader(file));
		JsonArray array = parse.getAsJsonArray();
		
		Iterator<JsonElement> itt = array.iterator();
		ArrayList<String> groups = new ArrayList<String>();
		groups.add("ISPWebItem");
		while (itt.hasNext()) {
			groups.add("ISPWebItem."+itt.next().getAsString());
		}

		itt = array.iterator();
		ArrayList<String> symbols = new ArrayList<String>();
		while (itt.hasNext()) {
			symbols.add(itt.next().getAsString());
		}

		result.setGroups(groups.toArray(new String[0]));
		result.setSymbols(symbols.toArray(new String[0]));

		return result;
	}
}