package com.alphadevs.isslive;

import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.mongodb.MongoException;

public class MongoDBPool {
	
	private DB db;
	
	private void init() throws UnknownHostException, MongoException{
		Mongo mongo=new Mongo();
		db = mongo.getDB("ISSLive");
	}
	
	private DBCollection getCollection(String string){
		return db.getCollection(string);
	}
	
	private MongoDBPool() throws UnknownHostException, MongoException {
		init();
	}
	
	private static MongoDBPool instance;
	
	public static MongoDBPool getInstance() throws UnknownHostException, MongoException{
		if(instance==null){
			instance=new MongoDBPool();
		}
		return instance;
	}
	
	public void insertData(JsonElement data, String symbol){
		getCollection(symbol).insert(jsonToBasicDBObject(data));
	}
	
	DBObject jsonToBasicDBObject(JsonElement data){
		DBObject obj;
		if(data.isJsonArray()){
			obj = new BasicDBList();
			JsonArray arr = (JsonArray) data;
			Iterator<JsonElement> itt = arr.iterator();
			while(itt.hasNext()){
				JsonElement k = itt.next();
				if(k.isJsonPrimitive()){
					((BasicDBList)obj).add(k.getAsString());
				}
				else{
					((BasicDBList)obj).add(jsonToBasicDBObject(k));
				}
				
			}
			return obj;
		}
		else{
			obj=new BasicDBObject();
			Set<Entry<String, JsonElement>> ent=((JsonObject)data).entrySet();
			for(Entry<String, JsonElement> entry : ent){
				if(entry.getValue().isJsonPrimitive()){
					((BasicDBObject)obj).put(entry.getKey(), entry.getValue().getAsString());
				}
				else{
					((BasicDBObject)obj).put(entry.getKey(), jsonToBasicDBObject(entry.getValue()));
				}
			}
			return obj;
		}
	}
}