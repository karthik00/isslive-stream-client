package com.alphadevs.isslive;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import com.lightstreamer.ls_client.ConnectionInfo;
import com.lightstreamer.ls_client.ExtendedConnectionListener;
import com.lightstreamer.ls_client.ExtendedTableInfo;
import com.lightstreamer.ls_client.LSClient;
import com.lightstreamer.ls_client.PushConnException;
import com.lightstreamer.ls_client.PushServerException;
import com.lightstreamer.ls_client.PushUserException;
import com.lightstreamer.ls_client.SubscrException;
import com.lightstreamer.ls_client.SubscribedTableKey;


/**
 * Client understands to connect to server and get the data from the stream
 * @author AlphaDevs
 *
 */

public class LightStreamerClient {

	private boolean serverRunning;

	ISSLightStreamerServerConfiguration serverConfiguration = null;

	private LSClient lsClient;

	private String configFilePath;

	private SubscribedTableKey tableRef;

	LightStreamerClient(String configFilePath) {
		this.configFilePath = configFilePath;
	}

	public LightStreamerClient() {
		this("/Users/karthik/dev/workspace/isslive-stream-client/src/com/alphadevs/isslive/lightstreamer.properites");
		//this(ClassLoader.getSystemClassLoader().getResource("lightstreamer.json").getPath());
		System.out.println();
	}

	public void start(ISSClientHandler issClientHandler)
			throws SubscrException, PushServerException, PushUserException,
			PushConnException {
		init();
		serverRunning = true;
		tableRef = lsClient.subscribeTable(new ExtendedTableInfo(
				issClientHandler.getConfig().getGroups(), "MERGE",
				issClientHandler.getConfig().getSymbols(), true),
				issClientHandler, false);
		bind(issClientHandler);
	}

	public void stop() {
		serverRunning = false;
	}

	private void bind(ISSClientHandler issClientHandler)
			throws SubscrException, PushServerException, PushConnException {
		while (serverRunning) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		lsClient.unsubscribeTable(tableRef);
	}

	private void init() {
		serverConfiguration = new ISSLightStreamerServerConfiguration();
		try {
			serverConfiguration.load(new FileInputStream(new File(
					configFilePath)));
			lsClient = new LSClient();

			try {
				lsClient.openConnection(new ConnectionInfo() {
					{
						this.pushServerUrl = "http://" + serverConfiguration.getHost();
						this.adapter = serverConfiguration.getTelemetryDataAdapter();
					}
				}, new ExtendedConnectionListener() {

					@Override
					public void onSessionStarted(boolean arg0) {
						System.out.println("Session started");
					}

					@Override
					public void onNewBytes(long arg0) {
						// System.out.println("Session started");
					}

					@Override
					public void onFailure(PushConnException e) {
						e.printStackTrace();
					}

					@Override
					public void onFailure(PushServerException e) {
						e.printStackTrace();
					}

					@Override
					public void onEnd(int arg) {
						System.out.println("Connection ended");
					}

					@Override
					public void onDataError(PushServerException e) {
						e.printStackTrace();
					}

					@Override
					public void onConnectionEstablished() {
						System.out.println("Connection Established");
					}

					@Override
					public void onClose() {
						System.out.println("Connection Closed");
					}

					@Override
					public void onActivityWarning(boolean arg0) {
						System.out.println("Warned");
					}

					@Override
					public void onSessionStarted(boolean arg0, String arg1) {
						System.out.println("Session started " + arg1);
					}
				});
			} catch (PushConnException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (PushServerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (PushUserException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			System.err
					.println("please give the input lightstreamer configuration file.");
		} catch (IOException e) {
			System.err.println(e);
		}
	}

}