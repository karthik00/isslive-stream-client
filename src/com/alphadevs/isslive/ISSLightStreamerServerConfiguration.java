package com.alphadevs.isslive;

import java.util.Properties;

/**
 * ISSLightStreamerServerConfiguration understands the server configuration to connect.
 * 
 * @author AlphaDevs
 *
 */

class ISSLightStreamerServerConfiguration extends Properties{
	
	private static final long serialVersionUID = 2484393691468398165L;
	
	public String getDomain(){
		return this.getProperty("domain");  
	  }
	  
	  public String getHost(){
		return this.getProperty("host");  
	  }
	  
	  public String getPort(){
		return this.getProperty("port");  
	  }
	  
	  public String getApplicationName(){
		return this.getProperty("applicationName");  
	  }
	  
	  public String getEnginePath(){
		return this.getProperty("enginePath");  
	  }
	  
	  public String getTelemetryDataAdapter(){
		return this.getProperty("telemetryDataAdapter");  
	  }
	  
	  public String getTimelineDataAdapter(){
		return this.getProperty("timelineDataAdapter");  
	  }
	  
	  public String getSymbolUpdateUrl(){
		  return this.getProperty("")+":"+this.getProperty("")+"/symbols/list";
	  }
}