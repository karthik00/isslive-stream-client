 package com.alphadevs.isslive;

import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.lightstreamer.ls_client.HandyTableListener;
import com.lightstreamer.ls_client.UpdateInfo;
import com.mongodb.MongoException;
 

/**
 * ISSClientHandler understands to handle the data from the socket and save to mongoDB.
 * 
 * @author AlphaDevs
 *
 */


 public class ISSClientHandler implements HandyTableListener{

	private final LSHandlerConfig config;
	private JsonParser p;
	private MongoDBPool db_instance;
	private Map<String, Double> symbolTimeStampMap;

	public ISSClientHandler(LSHandlerConfig config) throws UnknownHostException, MongoException {
		this.config = config;
		p=new JsonParser();
		db_instance=MongoDBPool.getInstance();
		this.symbolTimeStampMap=new HashMap<String,	Double>();
	 }

//     private String notifyValue(UpdateInfo update, String fldName) {
//         String notify = " " + fldName + " = " + update.getNewValue(fldName);
//         if (update.isValueChanged(fldName)) {
//         	notify += " (was " + update.getOldValue(fldName) + ")";
//         }
//         return notify;
//     }
     
     public void onUpdate(int itemPos, String itemName, UpdateInfo update){
    	 for(String key : getConfig().getSymbols()){
    		try {
    				if(!symbolTimeStampMap.containsKey(key)){
    					symbolTimeStampMap.put(key, new Double(0));
    				}
					JsonElement obj = p.parse(update.getNewValue(key));
					double i=obj.getAsJsonObject().get("Data").getAsJsonArray().get(0).getAsJsonObject().get("TimeStamp").getAsDouble();
					if(symbolTimeStampMap.get(key).doubleValue()!=i){
						obj.getAsJsonObject().addProperty("last_updated", new Double(i));
						db_instance.insertData(obj, key);
						symbolTimeStampMap.put(key, i);
						System.out.println(obj);
					}
			} catch (MongoException e) {
				e.printStackTrace();
			}
     	}
     }
     
     public void onSnapshotEnd(int itemPos, String itemName){
     	System.out.println("end of snapshot for " + itemName);
     }
     
     public void onRawUpdatesLost(int itemPos, String itemName, int lostUpdates) {
         System.out.println(lostUpdates + " updates lost for " + itemName);
     }
     
     public void onUnsubscr(int itemPos, String itemName) {
         System.out.println("unsubscr " + itemName);
     }

     public void onUnsubscrAll() {
         System.out.println("unsubscr table");
     }


	public LSHandlerConfig getConfig() {
		return config;
	}
 }